import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueAgile from 'vue-agile'




import Home from "../components/Home"
import AboutMe from "../components/AboutMe"
import Courses from "../components/Courses"
import Contact from "../components/Contact"
import Test from "../components/LingoTest/Test"




Vue.use(Router)
Vue.use(BootstrapVue)
Vue.use(VueAgile)



export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/AboutMe',
      name: 'AboutMe',
      component: AboutMe
    },
    {
      path: '/Courses',
      name: 'Courses',
      component: Courses
    },
    {
      path: '/Contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/Test',
      name: 'Test',
      component: Test
    }
  ]
})
